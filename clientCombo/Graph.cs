﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace combo
{
    public sealed class Graph : IGraph
    {
        private static Graph _graph;
        private Dictionary<Node, HashSet<Node>> structure;

        private Graph()
        {
            structure = new Dictionary<Node, HashSet<Node>>();
        }

        public static Graph GetInstance()
        {
            if (_graph == null)
            {
                _graph = new Graph();
            }
            return _graph;
        }

        public void AddConnection(Node origin, Node destination)
        {
            if (structure.ContainsKey(origin) && structure.ContainsKey(destination))
            {
                structure[origin].Add(destination);
                structure[destination].Add(origin);
            }
            throw new NullReferenceException();
        }

        public void AddNode(Node node)
        {
            if (!structure.ContainsKey(node))
            {
                structure.Add(node, new HashSet<Node>());
            }
        }

        public HashSet<Node> GetConnections(Node target)
        {
            if (structure.ContainsKey(target))
            {
                return structure[target];
            }
            throw new KeyNotFoundException();
        }

        public Dictionary<Node, HashSet<Node>> GetGraphStructure()
        {
            return structure;
        }

        public bool IsArchPresent(Node origin, Node destination)
        {
            return structure[origin].Contains(destination);

        }

        public bool IsNodePresent(Node node)
        {
            //if (structure.ContainsKey(node)) { return true; }
            foreach (Node n in structure.Keys)
            {
                if (Math.Abs(n.Centre.Key - node.Centre.Key) < (node.Radious / 2) && Math.Abs(n.Centre.Value - node.Centre.Value) < (node.Radious / 2))
                {
                    return true;
                }
            }
            return false;
        }

        public void RemoveConnection(Node origin, Node destination)
        {
            if (structure.ContainsKey(origin) && structure.ContainsKey(destination))
            {
                structure[origin].Remove(destination);
                structure[destination].Remove(origin);
            }
            throw new NullReferenceException();
        }

        public void RemoveNode(Node node)
        {
            if (structure.ContainsKey(node))
            {
                foreach (Node n in structure[node])
                {
                    structure[n].Remove(node);
                }
                structure.Remove(node);
            }
            throw new NullReferenceException();
        }
    }
}
