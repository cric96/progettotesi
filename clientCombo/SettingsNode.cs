﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace combo
{
    class SettingsNode
    {
        public string Fill { get; set; } = "Red";

        public string Stroke { get; set; } = "AliceBlue";

        public double Size { get; set; } = 60;


        public SettingsNode() { }


        public void SetSettings(SettingsNode set)
        {
            Fill = set.Fill;
            Stroke = set.Stroke;
            Size = set.Size;
        }

    }
}
