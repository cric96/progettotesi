﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace combo
{
    class NodeGraphics
    {
        public Node Base { get; private set; }

        public Ellipse Shape { get; private set; }

        public NodeGraphics(Node node, Ellipse el)
        {
            Base = node;
            Shape = el;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        #nullable enable
        public NodeGraphics? GetIfcointainsNode(Node n)
        {
            return Base.Equals(n) ? this : null;
        }

    }
}
