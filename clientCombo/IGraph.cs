﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace combo
{
    public interface IGraph
    {
        void AddNode(Node node);
        void RemoveNode(Node node);
        void AddConnection(Node origin, Node destination);
        void RemoveConnection(Node origin, Node destination);

        bool IsNodePresent(Node node);

        bool IsArchPresent(Node origin, Node destination);

        HashSet<Node> GetConnections(Node target);

        Dictionary<Node, HashSet<Node>> GetGraphStructure();
    }
}
