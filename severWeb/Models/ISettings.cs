﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace severWeb.Models
{
    public interface ISettings
    {
        SettingsNode SettingsNode{get; set;}

        void AddClientId(string id);

        bool ContainsClientId(string checkId);


    }
}
