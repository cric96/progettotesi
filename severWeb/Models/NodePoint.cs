﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace severWeb.Models
{
    public class NodePoint : IEquatable<Node>
    {
        public string Fill { get; set; }
        public string Stroke { get; set; }
        public double Radious { get; set; }
        public Point Centre { get; set; }
        public int Value { get; set; }
        public string Label { get; set; }
        public HashSet<Node> connectedTo { get; set; }

        public NodePoint() { }

        public NodePoint(string label, int value)
        {
            Label = label;
            Value = value;
        }

        public override string ToString()
        {
            return Label+Value;
        }

        public bool Equals(Node other)
        {
            return (this.Label.Equals(other.Label) && this.Value == other.Value);
        }

        public override int GetHashCode()
        {
            return Label.GetHashCode() ^ Value;
        }
    }
}