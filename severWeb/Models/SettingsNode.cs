﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace severWeb.Models
{
    public class SettingsNode : ISettingsNode
    {
     
        public string Fill { get; set; } = "Red";

        public string Stroke { get; set; } = "AliceBlue";

        public double Size { get; set; } = 60;


        public SettingsNode() { }


        public void SetSettings(SettingsNode set)
        {
            Fill = set.Fill;
            Stroke = set.Stroke;
            Size = set.Size;
        }


    }
}