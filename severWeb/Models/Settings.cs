﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace severWeb.Models
{
    public class Settings : ISettings
    {
        private static Settings _settings;
        private List<string> ClientsId;
        
        public SettingsNode SettingsNode { get ; set; }

        private Settings()
        {
            SettingsNode = new SettingsNode();
            ClientsId = new List<string>();
        }

        public static Settings GetInstance()
        {
            if(_settings == null)
            {
                _settings = new Settings();
            }
            return _settings;
        }

        public void AddClientId(string id)
        {
            ClientsId.Add(id);
        }

        public bool ContainsClientId(string checkId)
        {
            return ClientsId.Contains(checkId);
        }
    }
}