﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace severWeb.Models
{
    public sealed class Graph : IGraph
    {
        private static Graph _graph;
        private Dictionary<Node, HashSet<Node>> structure;

        private Graph()
        {
            structure = new Dictionary<Node, HashSet<Node>>();
        }

        public static Graph GetInstance()
        {
            if(_graph == null)
            {
                _graph = new Graph();
            }
            return _graph;
        }

        public void AddConnection(Node origin, Node destination)
        {
            structure[origin].Add(destination);
            structure[destination].Add(origin);
        }

        public void AddNode(Node node)
        {
            structure.Add(node, new HashSet<Node>());
        }

        public HashSet<Node> GetConnections(Node target)
        {
            return structure[target]; 
        }

        public Dictionary<Node,HashSet<Node>> GetGraphStructure()
        {
            return structure;
        }

        public bool IsArchPresent(Node origin, Node destination)
        {
            return structure[origin].Contains(destination);
            
        }

        public bool IsNodePresent(Node node)
        {
            foreach (Node n in structure.Keys)
            {
                if (Math.Abs(n.Centre.Key - node.Centre.Key) < (node.Radious / 2) && Math.Abs(n.Centre.Value - node.Centre.Value) < (node.Radious / 2))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsNodePresent(KeyValuePair<double, double> position)
        {
            foreach (Node n in structure.Keys)
            {
                if (Math.Abs(n.Centre.Key - position.Key) < (n.Radious / 2) && Math.Abs(n.Centre.Value - position.Value) < (n.Radious / 2))
                {
                    return true;
                }
            }
            return false;
        }

        public Node GetNode(KeyValuePair<double, double> position)
        {
            foreach (Node n in structure.Keys)
            {
                if (Math.Abs(n.Centre.Key - position.Key) < (n.Radious / 2) && Math.Abs(n.Centre.Value - position.Value) < (n.Radious / 2))
                {
                    return n;
                }
            }
            return new Node(); //TOFIX
        }

        public void RemoveConnection(Node origin, Node destination)
        {
            /*if (structure.ContainsKey(origin) && structure.ContainsKey(destination))
            {
                structure[origin].Remove(destination);
                structure[destination].Remove(origin);
            }
            throw new NullReferenceException();*/
            structure[origin].Remove(destination);
            structure[destination].Remove(origin);
        }

        public void RemoveNode(List<Node> indexes, Node target)
        {
            foreach(Node n in indexes)
            {
                structure[n].Remove(target);
            }
            structure.Remove(target);
            /*
            if (structure.ContainsKey(node))
            {
                foreach(Node n in structure[node])
                {
                    structure[n].Remove(node);
                }
                structure.Remove(node);
            }
            throw new NullReferenceException();
            */
        }

        
    }
}